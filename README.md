# Designing a Web Page | e-store request form

This is a basic form web page that sends an email with the details of the order of food to a certain email

## Functional Requirements:
* Order Form:
    * Users should be able to fill out their name and email address.
    * Users should be able to select items from the menu using checkboxes.
    * Users should be able to provide additional instructions or descriptions for their order.
    * Upon submitting the form, the order details should be sent via email to the restaurant.

## Non-functional Requirements:

* Usability:

The web page should be easy to use and navigate.
The form should have clear instructions and labels for each field.
* Performance:

The web page should load quickly to provide a seamless user experience.
Email sending should be efficient, with minimal delay.
* Reliability:

The web page and email sending functionality should be reliable and available at all times.

* Compatibility:

The web page should work well on different devices and web browsers.

--- 

## Images of the project

![Full page](resources/md/webpage1.png)
This is the full page view

![Full page](resources/md/webpage%20ordersent.png)
The forms filled with a custom order

When the success message appears, an email is sent using EmailJS.

![Email recieved](resources/md/webpage%20emailrecieved.png)

