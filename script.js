document.getElementById('orderForm').addEventListener('submit', function(event) {
    event.preventDefault();

    // Collect form data
    const name = document.getElementById('name').value;
    const email = document.getElementById('email').value;
    const food = document.getElementById('food').value;

    // Collect selected extra items
    const extraItems = [];
    document.querySelectorAll('input[name="extra-item"]:checked').forEach(function(item) {
        extraItems.push(item.value);
    });

    console.log(extraItems)
    // Prepare email data
    let message = '';
    if (extraItems.length > 0) {
        message += 'Items:\n' + extraItems.join(', ');
    }

    message += '\n\n' +`Order Description:\n${food}`

    const templateParams = {
        from_name: name,
        to_email: email,
        message: message
    };

    // Send email using EmailJS
    emailjs.send('service_xxxxxx', 'template_xxxxxx', templateParams)
        .then(function(response) {
            console.log('SUCCESS!', response.status, response.text);
            document.getElementById('responseMessage').textContent = 'Order sent successfully!';
            document.getElementById('responseMessage').style.color = 'green';
        }, function(error) {
            console.log('FAILED...', error);
            document.getElementById('responseMessage').textContent = 'Failed to send order. Please try again.';
            document.getElementById('responseMessage').style.color = 'red';
        });
});
